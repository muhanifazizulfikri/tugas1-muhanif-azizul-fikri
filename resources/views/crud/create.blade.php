@extends('tamplate')

@section('content')
    <h1> Tugas 1 Stupend Arkatama</h1>
    <h3>Biodata Mahasiswa</h3>
    <a href="/crud" class="btn btn-primary btn-sm mb-3"> Kembali</a>

    <form action="/crud" method="post">
        
        <div class="mb-3">
            <label for="">Nama</label>
            <input type="text" class="form-control" name="nama">
        </div>

        <div class="mb-3">
            <label for="">NPM</label>
            <input type="text" class="form-control" name="nim">
        </div>

        <div class="mb-3">
            <label for="">Jenis Kelamin</label>
            <select name="jenis kelamin" class="form-control">
                <option value="L">Laki-laki</option>
                <option value="P">Perempuan</option>
            </select>
        </div>

        <div class="mb-3">
            <label for="">prodi</label>
            <input type="text" class="form-control" name="prodi">
        </div>

        <div class="mb-3">
            <label for="">fakultas</label>
            <input type="text" class="form-control" name="fakultas">
        </div>
        <button class="btn btn-primary btn-sm" type="submit">Simpan</button>
    </form>
@endsection